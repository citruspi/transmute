//
//  TM_Drop_Files.h
//  Transmute
//
//  Created by Mihir Singh on 8/18/12.
//  Copyright (c) 2012 The Royal Panda Company. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TM_Drop_Files : NSImageView

- (void)runConversion:(id)sender;

@end
