//
//  main.m
//  Transmute
//
//  Created by Mihir Singh on 8/18/12.
//  Copyright (c) 2012 The Royal Panda Company. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
